from flask import current_app as app
from flask_marshmallow import Marshmallow
# !pip install flask_marshmallow
# !pip install marshmallow-sqlalchemy

ma = Marshmallow(app)


class TodoSchema(ma.Schema):
    class Meta:
        fields = ('id', 'content', 'date_created')


todo_schema = TodoSchema()
todos_schema = TodoSchema(many=True)
