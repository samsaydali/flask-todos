from flask import current_app as app, request, jsonify

with app.app_context():
    from models.Todo import Todo
    from models.TodoSchema import todos_schema
    from models.TodoSchema import todo_schema


@app.route('/', methods=['GET'])
def index():
    tasks = Todo.query.order_by(Todo.date_created).all()
    result = todos_schema.dump(tasks)
    return jsonify(result)


@app.route('/<int:id>', methods=['GET'])
def get(id):
    todo = Todo.query.get_or_404(id)
    result = todo_schema.dump(todo)
    return jsonify(result)


@app.route('/', methods=['POST'])
def create():
    content = request.form['content']
    todo = Todo(content=content)
    todo.save()
    result = todo_schema.dump(todo)
    return jsonify(result)


@app.route('/<int:id>', methods=['PUT'])
def update(id):
    todo = Todo.query.get_or_404(id)
    content = request.form['content']
    todo.content = content
    todo.update()
    result = todo_schema.dump(todo)
    return jsonify(result)


@app.route('/<int:id>', methods=['DELETE'])
def delete(id):
    todo = Todo.query.get_or_404(id)
    todo.delete()
    return jsonify(True)
